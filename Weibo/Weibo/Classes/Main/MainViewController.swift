//
//  MainViewController.swift
//  DSWeibo
//
//  Created by 唐宏勋 on 2017/4/29.
//  Copyright © 2017年 webJXCSs. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

       addChildViewControllers()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // 初始化加号按钮
        setupComposeBtn()
    }
    private func setupComposeBtn(){
        
        // 0.添加到tabBar上
        tabBar.addSubview(composeBtn)
        
        // 1.计算按钮宽度
        let width = tabBar.bounds.width / CGFloat(viewControllers!.count)
        // 2.创建按钮frame
        let rect = CGRect(x: 0, y: 0, width: width, height: tabBar.bounds.height)
        // 3.设置按钮frame和偏移位
        composeBtn.frame = rect.offsetBy(dx: width * 2, dy: 0)
    }
    private lazy var composeBtn:UIButton = {
        // 1.创建按钮
        let button = UIButton()
        // 2.设置图片
        button.setImage(UIImage(named: "tabbar_compose_icon_add"), for: UIControlState.normal)
        button.setImage(UIImage(named: "tabbar_compose_icon_add_highlighted"), for: UIControlState.highlighted)
        // 3.设置背景图片
        button.setBackgroundImage(UIImage(named: "tabbar_compose_button"), for: UIControlState.normal)
        button.setBackgroundImage(UIImage(named: "tabbar_compose_button_highlighted"), for: UIControlState.highlighted)
        
        // 4.监听加号按钮点击
        button.addTarget(self, action: #selector(MainViewController.composeBtnClick), for: UIControlEvents.touchUpInside)
        // 4.返回按钮
        return button
    }()
    func composeBtnClick(){
     
    }
    /**
     添加所有子控制
     */
    func addChildViewControllers() {
        tabBar.tintColor = UIColor.orange
        addChildViewController(childController: HomeTableViewController(),title:"首页",titles:"首页",imageName:"tabbar_home")
        
        addChildViewController(childController: MessageTableViewController(),title:"消息",titles:"消息",imageName:"tabbar_message_center")
         // 添加占位控制器
         addChildViewController(childController: NullViewController(),title:"消息",titles:"消息",imageName:"tabbar_message_center")
       
        addChildViewController(childController: DiscoverTableViewController(),title:"广场",titles:"广场",imageName:"tabbar_discover")
        
        addChildViewController(childController: ProfileTableViewController(),title:"我",titles:"我",imageName:"tabbar_profile")
    }
    private func addChildViewController(childController: UIViewController,title:String,titles:String,imageName:String) {
        // 0.动态获取命名空间
        let namespace = Bundle.main.infoDictionary!["Executable file"] as? String
        print(namespace)
        childController.tabBarItem.image = UIImage(named:imageName)
        childController.tabBarItem.selectedImage = UIImage(named:imageName+"_highlighted")
        childController.tabBarItem.title = title
        childController.navigationItem.title = titles
        let nav = UINavigationController()
        nav.addChildViewController(childController)
        addChildViewController(nav)
    }
}
